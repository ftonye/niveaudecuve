#importer les differents modules
from gpiozero import LED, Button
from signal import Pause

# 7-seg display pin assignment
#gpio14->a...gpio8->g
sega = LED(14)
segb = LED(15)
segc = LED(18)
segd = LED(23)
sege = LED(24)
segf = LED(25)
segg = LED(8)

#Electro-valves pin assignment
#gpio17->fill_valve_v , gpio27->purge_valve_r
fill_valve_v = LED(17)
purge_valve_r = LED(27)

#Liquid level sensors pin assignment
#gpio11->c1 ,gpio9->c2,gpio10->c3, gpio22->c4 
c1 = Button(11)
c2 = Button(9)
c3 = Button(10)
c4 = Button(22)

#this function displays the low_level alarm 
def Show_Low_Level_Alarm():
    sega.off()
    segb.off()
    segc.off()
    segd.on()
    sege.on()
    segf.on()
    segg.off()

#this function displays the high_level alarm 
def Show_high_Level_Alarm():
    sega.off()
    segb.on()
    segc.on()
    segd.off()
    sege.on()
    segf.on()
    segg.on()

#this function displays the level_1  
def ShowLevel_1():
    sega.off()
    segb.on()
    segc.on()
    segd.off()
    sege.off()
    segf.off()
    segg.off()

#this function displays the level_2  
def ShowLevel_2():
    sega.on()
    segb.on()
    segc.off()
    segd.on()
    sege.on()
    segf.off()
    segg.on()

#this function displays the level_3 
def ShowLevel_3():
    sega.on()
    segb.on()
    segc.on()
    segd.on()
    sege.off()
    segf.off()
    segg.on()